import os
import random
import string

from flask import Flask
from flask_socketio import SocketIO, join_room, emit

app = Flask(__name__)
socketio = SocketIO(app, cors_allowed_origins="*")
rooms = []
rooms_details = {}


@app.route('/')
def hello_world():
    return 'Hello, World!'


@socketio.on('join')
def on_join(data):
    client_id = data['clientId']
    data = data['data']
    username = data['username']

    room = find_available_room_and_join(client_id)
    join_room(room)
    if rooms_details[room]["clients"][0] == client_id:
        symbol = 'o'
        rooms_details[room]["turn"] = 'o'
        rooms_details[room]["table"] = [''] * 9
    else:
        symbol = 'x'
    emit('joinInfo', {'roomId': room, 'symbol': symbol, 'clientId': client_id})


def find_available_room_and_join(client_id):
    if rooms != [] and len(rooms_details[rooms[-1]]["clients"]) < 2:
        room_name = rooms[-1]
        rooms_details[room_name]["clients"].append(client_id)
    else:
        room_name = ''.join(random.choices(string.ascii_uppercase + string.digits, k=8))
        rooms.append(room_name)
        rooms_details[room_name] = {"clients": [client_id]}
    return room_name


@socketio.on('makeMove')
def moveRequest(data):
    client_id = data['clientId']
    data = data['data']
    roomId = data['roomId']
    symbol = data['symbol']
    cellId = data['cellId']
    if rooms_details[roomId]["turn"] == symbol and len(rooms_details[roomId]["clients"]) == 2:
        if symbol == 'x':
            rooms_details[roomId]["turn"] = 'o'
        else:
            rooms_details[roomId]["turn"] = 'x'
        rooms_details[roomId]["table"][int(cellId[4:])] = symbol
        emit('move', {'roomId': roomId, 'symbol': symbol, 'cellId': cellId})
        if is_someone_winning(rooms_details[roomId]["table"]):
            rooms_details[roomId]["turn"] = ''

            emit('win', {'clientId': client_id})
            emit('lost', {'clientId': next(x for x in rooms_details[roomId]["clients"] if x != client_id)})


def is_someone_winning(table):
    for i in range(0, 9, 3):
        if table[i] == table[i + 1] == table[i + 2] != '':
            return True

    for i in range(3):
        if table[i] == table[i + 3] == table[i + 6] != '':
            return True

    if table[0] == table[4] == table[8] != '' or table[2] == table[4] == table[6] != '':
        return True

    return False


if __name__ == '__main__':
    port = int(os.getenv('BACKEND_PORT', 5000))
    socketio.run(app, port=port, host='0.0.0.0', allow_unsafe_werkzeug=True)
