var socket = io.connect();
let roomId;
let symbol;

function connectToSocket() {
    const username = prompt("Wprowadź swoją nazwę użytkownika:");

    if (username) {
        socket.emit('join', {username: username});
        updateUsernameDisplay(username);
    } else {
        alert("Musisz podać swoją nazwę użytkownika!");
        connectToSocket();
    }
}

socket.on('connect', function() {
    connectToSocket();
});

socket.on('message', function(data) {
    console.log(data);
});

socket.on('joinInfo', function(data) {
    roomId = data.roomId;
    symbol = data.symbol;
    updateRoomIdDisplay(roomId);
});

socket.on('move', function(data) {
    if(data.roomId === roomId) {
        document.getElementById(data.cellId).innerText = data.symbol;
    }
});

socket.on('win', function() {
    updateGameResult("Wygrana");
});

socket.on('lost', function() {
    updateGameResult("Przegrana");
});

function handleClick(cell) {
    if(cell.innerText) return;
    socket.emit('makeMove', {roomId: roomId, symbol: symbol, cellId: cell.id});
}

function updateRoomIdDisplay(roomId) {
    document.getElementById('room-id').innerText = 'Room ID: ' + roomId;
}

function updateUsernameDisplay(username) {
    document.getElementById('username').innerText = 'Username: ' + username;
}

function updateGameResult(result) {
    document.getElementById('game-result').innerText = result;
    document.getElementById('game-result').style.visibility = 'visible'
}

document.addEventListener('DOMContentLoaded', () => {
    const boardElement = document.querySelector('.board');
    for(let i = 0; i < 9; i++) {
        let cell = document.createElement('div');
        cell.id = `cell${i}`;
        cell.className = 'cell';
        cell.onclick = () => handleClick(cell);
        boardElement.appendChild(cell);
    }
});
