const port = process.env.FRONTEND_PORT;

const express = require('express');
const http = require('http');
const socketIo = require('socket.io');
const socketIoClient = require('socket.io-client');
const app = express();
const server = http.createServer(app);
const io = socketIo(server);

const externalWsUrl = 'http://' + process.env.BACKEND_SERVICE_NAME + ':' + process.env.BACKEND_PORT;
const externalWs = socketIoClient(externalWsUrl);

io.on('connection', (clientSocket) => {
  const clientId = clientSocket.id
  clientSocket.join(clientId);

  clientSocket.on('disconnect', () => {
  });

  clientSocket.on('join', (data) => {
    externalWs.emit('join', {'data': data, 'clientId': clientId});
  });

  clientSocket.on('makeMove', (data) => {
    externalWs.emit('makeMove', {'data': data, 'clientId': clientId});
  });
});

externalWs.on('connect', () => {
  console.log('External WebSocket server connected');
});

externalWs.on('joinInfo', (data) => {
  io.to(data.clientId).emit('joinInfo', data);
});

externalWs.on('move', (data) => {
  io.emit('move', data);
});

externalWs.on('win', (data) => {
  io.to(data.clientId).emit('win');
});

externalWs.on('lost', (data) => {
  io.to(data.clientId).emit('lost');
});

app.use(express.static('public'));

server.listen(port, () => {
  console.log(`Server works on port: ${port}`);
  console.log(`Server connects to ${externalWsUrl} backend`);
});
